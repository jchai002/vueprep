#!/bin/bash
python ./vueprep/manage.py makemigrations
python ./vueprep/manage.py migrate
python ./vueprep/manage.py loaddata industries.json
python ./vueprep/manage.py loaddata criteria.json
python ./vueprep/manage.py loaddata questions.json
