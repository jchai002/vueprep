# VuePrep

- Online interview preparation software V0.01
- Domain name registered: www.vueprep.com
- Running on tensorflow backend

### To build an virt env

To create the environment:
```bash
conda create -n VuePrep python=3.6 tensorflow=2.3.0 numpy=1.18.5 pandas=1.0.5 opencv-python-headless=4.2.0.34 dlib=19.21.0 imutils=0.5.3 scikit-learn=0.21.3 moviepy=1.0.3 pyaudioanalysis=0.3.2
```
After creating the environment:
```bash
conda deactivate
source activate VuePrep
```

### Command line usage

```python 
python inference.py --filepath=IMG_5551.mp4
```