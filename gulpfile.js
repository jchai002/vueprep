const gulp = require("gulp");
const rollup = require("gulp-better-rollup");
const babel = require("rollup-plugin-babel");
const resolve = require("rollup-plugin-node-resolve");
const commonjs = require("rollup-plugin-commonjs");
const sass = require("gulp-sass");
const minify = require("gulp-minify-css");
const injectProcessEnv = require("rollup-plugin-inject-process-env");

const staticPath = "./vueprep/main_app/static/main_app/";
gulp.task("script", () => {
  return gulp
    .src(staticPath + "src/js/main.js")
    .pipe(
      rollup(
        {
          plugins: [
            babel(),
            resolve(),
            commonjs(),
            injectProcessEnv({
              NODE_ENV: process.env.NODE_ENV
            })
          ]
        },
        "umd"
      )
    )
    .pipe(gulp.dest(staticPath + "dst/"));
});

gulp.task("style", function() {
  return gulp
    .src([staticPath + "src/styles/**/*.scss"])
    .pipe(sass({ outputStyle: "compressed" }))
    .pipe(minify({ processImport: false }))
    .pipe(gulp.dest(staticPath + "dst/"));
});

gulp.task("watch", function() {
  gulp.watch([staticPath + "src/js/**/*.js"], gulp.parallel("script"));
  gulp.watch([staticPath + "src/styles/**/*.scss"], gulp.parallel("style"));
});

gulp.task("build", gulp.parallel("script", "style"));

gulp.task("default", gulp.parallel("script", "style", "watch"));
