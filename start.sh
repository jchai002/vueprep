#!/bin/bash
PWD=`pwd`
activate () {
    source $PWD/venv-vueprep/bin/activate
}
activate
pip install -r requirements.txt
python $PWD/vueprep/manage.py runserver
