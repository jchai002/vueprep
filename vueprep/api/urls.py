from django.urls import path
from . import routes

urlpatterns = [
    path('api/process-interview-question', routes.process_interview_question, name='api-process-interview-question'),
    path('api/predict-interview-question', routes.predict_interview_question, name='api-predict-interview-question'),
    path('api/transcribe-interview-question', routes.transcribe_interview_question, name='api-transcribe-interview-question'),
]
