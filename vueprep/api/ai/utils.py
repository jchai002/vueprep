#!/usr/bin/env python
# coding: utf-8

import cv2, os, operator, math
import base64, requests, collections
import pandas as pd
import numpy as np
from matplotlib import pyplot as plt
from sklearn.preprocessing import MinMaxScaler
from moviepy.editor import VideoFileClip
from moviepy.editor import AudioFileClip
from kapre.composed import get_melspectrogram_layer
from tensorflow.keras.models import Sequential
from tensorflow.keras import layers





# Use Affine transformation to rotate image
def Get_Rotated_Image(img,lt_eye_lt_corner,rt_eye_rt_corner,x=0.3,y=1/3,imsize=224):
    '''
    Use Affine Transformation to transform image
    Return
    ------
    Transformed image
    Transformation matrix
    '''
    # Initiate the size of output image
    w = h = imsize

    # Initiate the dst-eye_corner position
    eyecorner_dst = [(np.int((x)   * w), np.int(y * h)),
                     (np.int((1-x) * w), np.int(y * h))]
    
    # Initiate the src-eye_corner position
    eyecorner_src = [tuple(lt_eye_lt_corner), 
                     tuple(rt_eye_rt_corner)]

    # Compute similarity transform from src-eye_corner to dst-eye_corner
    # Note that all these calculations are done to get a fake point...
    # However it is crucial because you cannot get the third point from landmark
    # This is because the ratio of the trianglular points you get is different
    # For every body. Therefore you need to get a third point based on calculations
    s60 = math.sin(60*math.pi/180)
    c60 = math.cos(60*math.pi/180) 

    inPts = np.copy(eyecorner_src).tolist()
    outPts = np.copy(eyecorner_dst).tolist()

    xin = c60*(inPts[0][0] - inPts[1][0]) - s60*(inPts[0][1] - inPts[1][1]) + inPts[1][0]
    yin = s60*(inPts[0][0] - inPts[1][0]) + c60*(inPts[0][1] - inPts[1][1]) + inPts[1][1]

    inPts.append([np.int(xin), np.int(yin)])

    xout = c60*(outPts[0][0] - outPts[1][0]) - s60*(outPts[0][1] - outPts[1][1]) + outPts[1][0]
    yout = s60*(outPts[0][0] - outPts[1][0]) + c60*(outPts[0][1] - outPts[1][1]) + outPts[1][1]

    outPts.append([np.int(xout), np.int(yout)])

    # this is where the real transformation happens
    tform = cv2.estimateAffine2D(np.array([inPts]), np.array([outPts]))

    # the third point is transformed to nothing so we ignore it
    tform = tform[0]
    tform = np.float32(tform.flatten()[:6].reshape(2,3))

    # Apply similarity transform on image
    img = cv2.warpAffine(src = img, M = tform, dsize=(w, h))
    
    return img





# Flatten a nested dictionary
def Get_Flattened_Dict(d, parent_key='', sep='_'):
    '''A utility function that flattens Face++'s
    nested dictionary object and returns a flattened dictionary
    '''
    items = []
    for k, v in d.items():
        new_key = parent_key + sep + k if parent_key else k
        if isinstance(v, collections.abc.MutableMapping):
            items.extend(Get_Flattened_Dict(v, new_key, sep=sep).items())
        else:
            items.append((new_key, v))
    return dict(items)

# Get distance between two points in any dimensional space
def Get_Euclidean_Distance(source_representation, test_representation):
    '''Calculate the Euclidean 
    distance between two vectors:
    1) source_representation (any dimension)
    2) test_representation (any dimension)
    Parameter
    ---------
    src_representation  : Numpy Array
    test_representation : Numpy Array
    
    Return
    ------
    Int
    '''
    # Find the difference bewteen each dimension
    euclidean_distance = source_representation - test_representation
    
    # Square the differences and add them together
    euclidean_distance = np.sum(np.multiply(euclidean_distance, euclidean_distance))
    
    # Finally get the square root and return
    return np.sqrt(euclidean_distance)

# Face++ request function and check whether the face is neutral and straight
def Get_FacePlusPlus_Outputs(img, landmark_106 = 2, compare_face = 10, free_key = True):
    '''
    Params
    ------
    img          : numpy array of shape (n,n,3)
    landmark_106 : 2 if 106-D landmarks
    compare_face : 1 or any number
                   if face_num>compare_face:
                       return 0,0,0
                   basically if there are more than one face,
                   then it is not what we want. However, if 
                   you know that the face you want is bigger
                   than all other faces (i.e. in the foreground),
                   you can set compare face to be more than 5
                   so that the program can compare all faces and 
                   return only the biggest face. 
    Return
    ------
    Features of only one face:
    
    (score, landmarks, attributes) if face detected
    (0, 0, 0) if no face detected
    '''
    
    # Convert img from array to string then to base64
    img_str = cv2.imencode('.jpg', img)[-1]
    img_64 = base64.b64encode(img_str)
    
    # Prepare to drop the package to face++
    http_url = 'https://api-us.faceplusplus.com/facepp/v3/detect';
    re_att = 'gender,age,smiling,headpose,eyestatus,emotion,ethnicity,eyegaze,beauty,mouthstatus,blur'
    key = "cjf6uSKR0oJAs6Gcwrh8PPSFzEATZjOa" if free_key == True else "vJ_H5vJ0vuS_w7wZtmnBQC03PQluKoBZ"
    secret = "REithMdH9Bfbxy4oGTGK7flkD5AKekT-" if free_key == True else "uX3DJGJKEs64E53xpQQG9Zb9tZKSopxq"
    payload = {'api_key': key,
               'api_secret': secret,
               'image_base64': img_64,
               'return_landmark': landmark_106,
               'return_attributes': re_att}
    
    # Upload the package using post (not get)
    # If requests successful continue with further analysis
    try:
        # Request Face++, add timeout because it can get stuck
        response = requests.post(http_url, data=payload, timeout=5)
        face_num = response.json()['face_num']
        response = response.json()['faces']

        distances = {}
        responses = []
        face_count = 0

        if face_num>compare_face:
            return 0,0,0
        else:
            # Face++ returns a list of dictionaries
            for face in response:

                # Since face++ only returns the largest 5 faces
                if face_count<5:

                    # Make response into a better format for landmarks and attributes
                    landmarks = {i:(v['x'],v['y']) for i,v in face['landmark'].items()}
                    attributes = Get_Flattened_Dict(face['attributes'])

                    # Check occlusions and head pose according to Wang & Kosinski (2018) P.248
                    score = (attributes['blur_blurness_value'] <= 5) + \
                    (attributes['mouthstatus_surgical_mask_or_respirator'] <= 5) + \
                    (attributes['mouthstatus_other_occlusion'] <= 5) + \
                    ((attributes['eyestatus_left_eye_status_occlusion'] <= 5) or \
                     (attributes['eyestatus_right_eye_status_occlusion'] <= 5)) + \
                    (abs(attributes['headpose_pitch_angle']) <= 10) + \
                    (abs(attributes['headpose_yaw_angle']) <= 15)

                    # Check size of face by getting the length between eyes
                    distance = int(Get_Euclidean_Distance(np.asarray(landmarks['left_eye_left_corner']),
                                                          np.asarray(landmarks['right_eye_right_corner'])))

                    # Append distance into distances dict
                    distances[face_count]=distance

                    # Append all into a list 
                    responses.append((score,landmarks,attributes))

                    # Face count adds one because 
                    # I don't want it to read the no.6 and more 
                    # Face because it returns nothing
                    face_count+=1

            # Finally, if no distance detected
            if len(distances)==0:
                return 0,0,0
            # If there is at least one distance
            else:
                # Check the maximum distance and return position of key
                selected_key = max(distances.items(), 
                                   key=operator.itemgetter(1))[0]

                # Unpack the best face using selected key position
                final_score = responses[selected_key][0]
                final_landmarks = responses[selected_key][1]
                final_attributes = responses[selected_key][2]

                # Return score, landmarks, attributes in this order
                return final_score, final_landmarks, final_attributes

        # If for some reason unsuccessful
        # E.g. timeout after 2 seconds
        # Or no 'faces' key found in response
    except:
        # Return nothing
        return 0,0,0


    
    
    
# video / audio utilities    
def open_video(src_fp,FPS=5,SR=22050):
    
    with VideoFileClip(src_fp,audio=False) as clip:
        v_array=np.array(list(clip.iter_frames(fps=FPS)))
        v_array=(v_array).astype(np.uint8)

    with AudioFileClip(src_fp,math.inf,2,SR) as clip:
        a_array=np.array(clip.to_soundarray())
        a_array=(a_array.sum(axis=1)/2).astype(np.float64)
    
    return v_array,a_array

def get_window(v_array,a_array,
               DT=10,FPS=5,SR=22050):
    
    duration=min(len(v_array)//FPS,len(a_array)//SR)
    
    if duration==DT:
        
        v_array_out=v_array[:DT*FPS]
        a_array_out=a_array[:DT*SR]
    
    elif duration<DT:
        
        start=np.random.randint(0,DT-duration)
        a_zeros=np.zeros((SR*DT,),dtype=np.float64)
        v_zeros=np.zeros([DT*FPS]+list(v_array.shape[1:]),dtype=np.float64)

        a_zeros[start*SR:(start+duration)*SR]=a_array[:duration*SR]
        v_zeros[start*FPS:(start+duration)*FPS]=v_array[:duration*FPS]

        v_array_out=v_zeros
        a_array_out=a_zeros
    
    elif duration>DT:
        
        start=np.random.randint(0,duration-DT)
        v_array_out=v_array[start*FPS:(start+DT)*FPS]
        a_array_out=a_array[start*SR:(start+DT)*SR]
    
    return v_array_out,a_array_out

# for extracting mel-spectrogram
def build_spect_model(SR=22050,DT=10,TYPE='Mel',n_fft=2048):
    
    model=Sequential()
    spectrum = get_melspectrogram_layer(
        input_shape=(SR*DT,1),
        n_mels=200,
        n_fft=int(n_fft),
        win_length=441,
        hop_length=147,
        sample_rate=SR,
        return_decibel=True,
        input_data_format='channels_last',
        output_data_format='channels_last')
    model.add(spectrum)
    model.add(layers.LayerNormalization(axis=2))
    
    return model

def predict_spect(audio,model):
    
    batch = np.expand_dims(audio,axis=0)
    spect = model(batch).numpy().squeeze().T
    spect = MinMaxScaler().fit_transform(spect)
    
    return spect.astype(np.float64)

def visualize_audio(a,s,savefig=False):
    
    f,ax=plt.subplots(1,1,figsize=(40,2),frameon=False)
    ax.plot([np.mean(a[i:i+64]) for i in range(0,a.shape[0],64)])
    ax.axis('off');ax.margins(x=0,y=0)
    
    f,ax=plt.subplots(1,1,figsize=(40,5),frameon=False)
    ax.imshow(s,cmap='inferno')
    ax.axis('off');ax.margins(x=0,y=0)
    plt.tight_layout();plt.show();plt.close()
    
def visualize_video(v,IMSIZE=128,N_IMGS=20,savefig=False):
    
    film_strip=np.zeros((IMSIZE,IMSIZE*N_IMGS,3),dtype=np.uint8)
    for count,frame in enumerate(sorted(
        np.random.randint(0,len(v),N_IMGS))):
        img=cv2.resize(v[frame],(IMSIZE,IMSIZE))
        film_strip[:,count*IMSIZE:(count+1)*IMSIZE,:]=img
        
    f,ax=plt.subplots(1,1,figsize=(40,5),frameon=False)
    ax.imshow(film_strip)
    ax.axis('off');ax.margins(x=0,y=0)
    plt.tight_layout();plt.show();plt.close()
    
def get_distance(pt1,pt2):
    x1,y1=pt1
    x2,y2=pt2
    return math.sqrt((x2-x1)**2+(y2-y1)**2)

def get_midpoint(pt1,pt2):
    x1,y1=pt1
    x2,y2=pt2
    return (int((x1+x2)/2),int((y1+y2)/2))

def get_fwhr_FPP(landmarks):
    
    if landmarks is not 0:
        lt_eye = landmarks['left_eye_left_corner']
        rt_eye = landmarks['right_eye_right_corner']
        ct_eye = get_midpoint(lt_eye,rt_eye)
        lt_mth = landmarks['mouth_left_corner']
        rt_mth = landmarks['mouth_right_corner']
        ct_mth = get_midpoint(lt_mth,rt_mth)
        width  = get_distance(lt_eye,rt_eye)
        height = get_distance(ct_eye,ct_mth)
        fwhr   = width/height 
        return lt_eye,rt_eye,fwhr
    else:
        return None,None,None
        

# align face using either Face++ (online) or MTCNN (local)
def align_face(v_array,IMSIZE=224):
    
    stats=[]
    attrs=[]
    score=[]
    
    for count,frame in enumerate(v_array):
        
        # try with free key first then try with paid key
        score_,landmarks,attribute=Get_FacePlusPlus_Outputs(frame,free_key=True)
        
        if attribute is not 0:
            status = 'using free key'
            attrs.append(attribute)
            stats.append(get_fwhr_FPP(landmarks))
            score.append(score_)
        else:
            score_,landmarks,attribute=Get_FacePlusPlus_Outputs(frame,free_key=False)
            status = 'using paid key'
            attrs.append(attribute)
            stats.append(get_fwhr_FPP(landmarks))
            score.append(score_)
        print(f'Preprocessing frame: {count+1}/{len(v_array)} | {status}',end='\r')
    stats=pd.DataFrame(stats,columns=['lt','rt','fwhr'])
    mean=stats[['fwhr']].mean()[0] ; faces=[]

    for count,frame in enumerate(v_array):
        lt=stats.loc[count]['lt']
        rt=stats.loc[count]['rt']
        fwhr=stats.loc[count]['fwhr']

        # make sure face is detected
        if lt is not None:

            # generate scale factor using fwhr
            scale=(2*mean-fwhr)/mean
            
            if (scale>1.04) and (scale<=1.3):
                x_ratio = 0.31*(np.power(scale,0.4))
            elif scale>1.3:
                x_ratio = 0.31*(np.power(scale,0.65))
            else:
                x_ratio = 0.31
   
            face=Get_Rotated_Image(frame,lt,rt,x_ratio,0.31,IMSIZE)

            faces.append(face)
        
        # get the last frame
        elif len(faces)>0:
            faces.append(faces[-1])
        
        # else pad with zeros
        else:
            faces.append(np.zeros((IMSIZE,IMSIZE,3)))
            
    values=[]
    for v in attrs:
        if v!=0:
            keys=sorted(v.keys())
            v_=[]
            for i in keys:
                v_.append(v[i])
            values.append(v_)
        else:
            values.append([np.NaN]*50)
    df = pd.DataFrame(values,columns=keys).replace({'Female':1,'Male':0})
    
    return np.array(faces),df,np.mean(score)

def preprocess_video(src_fp,SR=22050,FPS=2,DT=15,IMSIZE=224,plot=True):
    '''
    Params
    ------
    src_fp  : './Data/sample_videos/10005551.mp4'
    SR      : sampling rate (default is 22050 per sec)
    FPS     : frame per second (downsize to 2 to 5)
    DT      : data time in seconds
    IMSIZE  : img.shape = (IMSIZE,IMSIZE,3)
    plot    : whether to plot graph
    Return
    ------
    final_data : [{video,audio,spect,attrb,score},{...},{...}]
    '''
    
    final_data = []
    
    v_array,a_array = open_video(src_fp,FPS=FPS,SR=SR)
    duration = int(min(len(v_array)//FPS,len(a_array)//SR))
    chunk_size = int(duration) if duration < 30 else int(duration//2) if duration < 60 else int(duration//3)
    
    if duration > 7:        
        for offset in range(0,duration,chunk_size):
            if duration - offset > 7:
                video = v_array[offset*FPS : offset*FPS + chunk_size*FPS]
                audio = a_array[offset*SR  : offset*SR  + chunk_size*SR]
                video,audio = get_window(video,audio,DT=DT,FPS=FPS,SR=SR)
                spect = predict_spect(audio,build_spect_model(SR=SR,DT=DT))
                video,attrb,score = align_face(video,IMSIZE=IMSIZE)
                final_data.append({'video':(video/255).astype(np.float64),
                                   'audio':audio,
                                   'spect':spect.astype(np.float64).T,
                                   'attrb':attrb,
                                   'score':score})
                if plot==True:
                    visualize_audio(audio,spect)
                    visualize_video(video)
        return final_data
    else: 
        return None