import json, os
import numpy as np
import pandas as pd

def avg_json(json_array):
    recording_quality = {}
    facial_attributes = {}
    personality_traits = {}
    # expression = {}
    # expressions = ['anger','disgust','fear','happiness','neutral','sadness','surprise']

    for n,v in enumerate(json_array):
        if v:
            recording_quality[n] = json.loads(v)['recording_quality']
            facial_attributes[n] = json.loads(v)['facial_attributes']
            personality_traits[n] = json.loads(v)['personality_traits']
            # expression[n] = expressions[int(facial_attributes[n]['emotion'])]

    recording_quality = dict(pd.DataFrame.from_dict(recording_quality).mean(axis=1))
    facial_attributes = dict(pd.DataFrame.from_dict(facial_attributes).mean(axis=1))
    personality_traits = dict(pd.DataFrame.from_dict(personality_traits).mean(axis=1))
    # overall_score = get_score_grade(personality_traits['overall'])
    return {"recording_quality":recording_quality, "facial_attributes": facial_attributes, "personality_traits": personality_traits}

def get_interpretation(prediction):
    prediction = json.loads(prediction)
    opening_sentence = {0:['Great job!','Excellent response!','You aced it!',
                           'Amazing job!','What a wonderful response!'],
                        1:['Great effort!', 'Good effort!','Excellent effort!',
                           'It was a great practice session!','Great session!','You are doing great!']}
    connect_sentence = {0:['You left a lasting impression.','You left a great impression.',
                           'You gave a wonderful impression.','People would be impressed by your personality.'],
                        1:['However, you might want to consider improving on the following aspects:']}

    emo_reason = 'Looking confident and relaxed is highly correlated with warmth, friendliness and trustworthiness. \
    Conversely, looking nervous or tense may misleadingly cause others to perceive you as unfriendly, \
    lacking trustworthiness or neurotic, which might in turn affect your chances of getting hired. '
    emo_improv = 'Dressing smart, making yourself comfortable, deep breaths or quick meditation \
    before the interview session starts may help. Most of all, believe in yourself!'

    emotion_sentence = {0:[emo_reason+'You seem to be very tensed up. Try to relax more! '+emo_improv,
                           emo_reason+'You look like you were quite nervous. Try to loosen up a bit! '+emo_improv],
                        1:[emo_reason+'You seem to be slightly tensed up. Try to relax more! '+emo_improv,
                           emo_reason+'You look like you were slightly nervous. Try to loosen up a bit! '+emo_improv],
                        2:[emo_reason+'However, you were looking pretty confident!'],
                        3:[emo_reason+'However, you were looking pretty confident!']}

    exp_reason = 'Facial expressions are extremely crucial in the first few seconds for building lasting \
    relationships when you meet a new person. Have you heard of flight or fight response? Similarly, humans evolve \
    to formulate approach or avoid tendencies based on the other person\'s warmth and dominance, which is largely \
    signaled through the other person\'s facial expressions! In plain words, if you are not smiling, the other \
    employer would be less likely to approach you for a job. '

    express_sentence = {0:[exp_reason+'Thus, try to put on a smile when you talk!'],
                        1:[exp_reason+'You gave a great smile! Keep it up!'],
                        2:[exp_reason+'You gave a great smile! Keep it up!'],
                        3:[exp_reason+'You gave a great smile! Keep it up!']}

    res = f"{np.random.choice(opening_sentence[int(prediction['personality_traits']['overall']//50)])} {np.random.choice(connect_sentence[int(prediction['personality_traits']['overall']//50)])} {np.random.choice(emotion_sentence[int((prediction['personality_traits']['neuroticism']//30+ prediction['personality_traits']['extraversion']//30)/2)])} {np.random.choice(express_sentence[int(prediction['facial_attributes']['smile']//30)])}"
    return ' '.join(res.split())
