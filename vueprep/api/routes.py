import os
from vueprep.settings import ERROR_STRING, STATUS, QUESTIONS_PER_INTERVIEW
from django.http import JsonResponse
from django.apps import apps
from django.forms.models import model_to_dict
from main_app.concerns.decorators import csrf_exempt_except_prod
from api.concerns.upload_and_predict import upload_and_predict
from api.ai.inference import predictions_driver
from api.concerns.helpers import download_from_url
from api.concerns.video_to_text import video_to_text


InterviewQuestion = apps.get_model('main_app', 'InterviewQuestion')

@csrf_exempt_except_prod
def process_interview_question(request):
    if request.method == 'POST':
        if 'video' not in request.FILES:
            print('No file detected')
            return JsonResponse({"msg":"No file detected"})
        if 'q_id' not in request.POST:
            print('No Question ID detected')
            return JsonResponse({"msg":"No Question ID detected"})
        if 'i_id' not in request.POST:
            print('No Interviw ID detected')
            return JsonResponse({"msg":"No Interviw ID detected"})
       
        options = { 'should_predict': False, 'should_transcribe': False }
        if 'should_predict' in request.POST:
           options['should_predict'] = True

        if 'should_transcribe' in request.POST:
           options['should_transcribe'] = True

        # most of the heavy lifting is done by upload_and_predict, very slow function with multiple threads
        data = upload_and_predict(request.FILES['video'], request.FILES['thumb'], options)
        
        params = { 'i_id': request.POST['i_id'], 'q_id': request.POST['q_id'], 'data': data}
        # create interview question
        i_q = InterviewQuestion.create_interview_question(params, options)
        # check if interview is COMPLETE, if so combine results
        i = i_q.interview
        if i.questions.count() == i.valid_question_count:
            i.combine_results()
        if i_q.score == ERROR_STRING:
            return JsonResponse({ 'error': f'error processing q_id: {request.POST["q_id"]}'})
        return JsonResponse({ 'msg': f'finished processing q_id: {request.POST["q_id"]}'})

@csrf_exempt_except_prod
def predict_interview_question(request):
    if request.method == 'POST':
        if 'i_q_id' not in request.POST:
            print('No interview question detected')
            return JsonResponse({"msg":"No interview question detected"})
        i_q = InterviewQuestion.objects.get(pk=int(request.POST[ 'i_q_id']))
        fp = download_from_url(i_q.video_url)
        prediction = predictions_driver(fp)
        i_q.set_data(prediction)
        os.remove(fp)
    if i_q.score == ERROR_STRING:
        return JsonResponse({ 'interview_question': {'error': f'error processing i_q_id: {request.POST["i_q_id"]}'}, 'interview': {'error': f'interview not updated'}})
    i = i_q.interview
    i.combine_results() 
    return JsonResponse({ 'interview_question': model_to_dict(i_q, fields= ['id', 'score', 'grade', 'prediction']), 'interview': model_to_dict(i, fields= ['id', 'score', 'grade', 'interpretation']) })

@csrf_exempt_except_prod
def transcribe_interview_question(request):
    if request.method == 'POST':
        if 'i_q_id' not in request.POST:
            print('No interview question detected')
            return JsonResponse({"msg":"No interview question detected"})
        i_q = InterviewQuestion.objects.get(pk=int(request.POST[ 'i_q_id']))
        fp = download_from_url(i_q.video_url)
        i_q.transcript = video_to_text(fp) 
        i_q.save()
        os.remove(fp)
    return JsonResponse({ 'transcript': i_q.transcript })