import os, requests
from vueprep.settings import UPLOAD_FOLDER, DOWNLOAD_FOLDER
from werkzeug.utils import secure_filename
from datetime import datetime, timezone
from django.core.files.storage import FileSystemStorage

def current_timestamp():
    return str(datetime.now(timezone.utc).timestamp()).replace('.', '')

def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in {'mp4', 'mov', 'avi', 'wav', 'webm'}

def add_video_metadata(video):
    filename = current_timestamp() + "_" + secure_filename(video.name)
    fs = FileSystemStorage(location=UPLOAD_FOLDER)
    fs.save(filename, video)
    fp_no_meta = os.path.join(UPLOAD_FOLDER, filename)
    # HACK: use npm module to add metadata
    new_fp = os.path.join(UPLOAD_FOLDER, "seekable_" + filename)
    os.system("ts-ebml -s " + fp_no_meta + " | cat >" + new_fp)
    # remove original file because it's not needed now
    os.remove(fp_no_meta)
    return new_fp

def download_from_url(url: str):
    filename = url.split('/')[-1].replace(" ", "_")  # be careful with file names
    fp = os.path.join(DOWNLOAD_FOLDER, filename)

    r = requests.get(url, stream=True)
    if r.ok:
        print("saving to", os.path.abspath(fp))
        with open(fp, 'wb') as f:
            for chunk in r.iter_content(chunk_size=1024 * 8):
                if chunk:
                    f.write(chunk)
                    f.flush()
                    os.fsync(f.fileno())
    return fp