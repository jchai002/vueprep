import os, moviepy.editor
from .aws import s3_upload, transcribe

def video_to_text(video_fp):
    print("video_to_text")
    video_ext = video_fp.split(".")[-1]
    audio = moviepy.editor.VideoFileClip(video_fp).audio
    audio_fp = video_fp.replace(video_ext, "mp3")
    audio.write_audiofile(audio_fp)
    print(audio_fp)
    # upload to s3
    audio_url = s3_upload(audio_fp, "audio")
    os.remove(audio_fp)
    return transcribe(audio_url)
