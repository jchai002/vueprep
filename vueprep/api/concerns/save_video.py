import os
from .helpers import allowed_file, add_video_metadata

def save_video(video):
    print("save_video")
    if video.name == '':
        print('No selected file')
        return "No selected file"
    if video and allowed_file(video.name):
        return add_video_metadata(video)
