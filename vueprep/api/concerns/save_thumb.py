import os
from werkzeug.utils import secure_filename
from vueprep.settings import UPLOAD_FOLDER
from django.core.files.storage import FileSystemStorage
from .helpers import current_timestamp

def save_thumb(thumb):
    thumb_filename = current_timestamp() + "_" + secure_filename(thumb.name)
    fs = FileSystemStorage(location=UPLOAD_FOLDER)
    fs.save(thumb_filename, thumb)
    thumb_filepath = os.path.join(UPLOAD_FOLDER, thumb_filename)
    return thumb_filepath
