import os, logging
from concurrent.futures import ThreadPoolExecutor
from .aws import s3_upload
from .save_video import save_video
from .save_thumb import save_thumb
from .video_to_text import video_to_text
from vueprep.settings import ERROR_STRING
from api.ai.inference import predictions_driver

def upload_and_predict(video, thumb, options):
    res = { 'video_url':'', 'thumb_url':'', 'prediction':'', 'transcript':'' }
    executorDict = { 'video_url':'', 'thumb_url':'', 'prediction':'', 'transcript':'' }
    video_fp = save_video(video)
    thumb_fp = save_thumb(thumb)
    with ThreadPoolExecutor(max_workers=4) as executor:
        # task 1
        try:
            executorDict['video_url'] = executor.submit(s3_upload, video_fp, "video")
        except Exception as e:
            logging.error(e)
            res['video_url'] = ERROR_STRING
        # task 2
        try:
            executorDict['thumb_url'] = executor.submit(s3_upload, thumb_fp, "thumb")
        except Exception as e:
            logging.error(e)
            res['thumb_url'] = ERROR_STRING
        # task 3
        if options['should_predict'] == True:
            try:
                executorDict['prediction'] = executor.submit(predictions_driver, video_fp)
            except Exception as e:
                logging.error(e)
                res['prediction'] = ERROR_STRING
        # task 4
        if options['should_transcribe'] == True:
            try:
                executorDict['transcript'] = executor.submit(video_to_text, video_fp)
            except Exception as e:
                logging.error(e)
                res['transcript'] = ERROR_STRING

    res['video_url'] = executorDict['video_url'].result()
    res['thumb_url'] = executorDict['thumb_url'].result()
    if options['should_predict'] == True:
        res['prediction'] = executorDict['prediction'].result()
    if options['should_transcribe'] == True:
        res['transcript'] = executorDict['transcript'].result()

    # clean up temp storage
    os.remove(video_fp)
    os.remove(thumb_fp)

    return res
