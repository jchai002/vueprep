import os, time, logging, requests, boto3
from botocore.exceptions import ClientError
from vueprep.settings import PROJECT_ENV
from .helpers import current_timestamp
import random

def s3_upload(fp, subdir_name):
    print('upload to s3')
    object_name = os.path.basename(fp)
    bucket_name = "vueprep"
    obj_key = subdir_name + '/' + object_name
    file_ext = object_name.split(".")[-1]
    # Upload the file
    ses = boto3.session.Session(
        aws_access_key_id = PROJECT_ENV("aws_access_key_id"),
        aws_secret_access_key = PROJECT_ENV("aws_secret_access_key"),
        region_name = 'us-west-1'
    )
    s3_client = ses.client('s3')

    location = s3_client.get_bucket_location(Bucket=bucket_name)['LocationConstraint']
    try:
        s3_client.upload_file(fp, bucket_name, obj_key, ExtraArgs={ 'ContentType': 'video/' + file_ext})
    except ClientError as e:
        logging.error(e)
        return False
    url = "https://s3-%s.amazonaws.com/%s/%s" % (location, bucket_name, obj_key)
    print(f's3 done: {subdir_name} {url}')
    return url


def transcribe(url):
    ses = boto3.session.Session(
        aws_access_key_id = PROJECT_ENV("aws_access_key_id"),
        aws_secret_access_key = PROJECT_ENV("aws_secret_access_key"),
        region_name = 'us-west-1'
    )
    transcribe_client = ses.client('transcribe')
    job_name = current_timestamp()
    job_uri = url
    transcribe_client.start_transcription_job(
        TranscriptionJobName = job_name,
        Media = { 'MediaFileUri': job_uri },
        MediaFormat='mp3',
        LanguageCode='en-US'
    )
    while True:
        status = transcribe_client.get_transcription_job(TranscriptionJobName = job_name)
        if status['TranscriptionJob']['TranscriptionJobStatus'] in ['COMPLETED', 'FAILED']:
            break
        print("waiting for transcript...")
        time.sleep(5)
    transcript_url = status['TranscriptionJob']['Transcript']['TranscriptFileUri']
    res = requests.get(transcript_url).json()['results']['transcripts'][0]['transcript']
    print(res)
    return res
