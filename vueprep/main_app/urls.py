from django.urls import path, re_path
from . import views
from django.contrib.auth import views as auth_views

urlpatterns = [
    path('', views.home, name='main_app-home'),
    path(r'interview_details/<int:id>', views.interview_details, name='main_app-interview_details'),
    path('mock-interview', views.mock_interview, name='main_app-mock_interview'),
    path('interview-question', views.interview_question, name='main_app-interview_question'),
    path('register', views.register, name='register'),
    path('login', auth_views.LoginView.as_view(template_name='main_app/login.html'), name='login'),
    path('logout', auth_views.LogoutView.as_view(template_name='main_app/home.html'), name='logout'),
    path('dashboard', views.dashboard, name='main_app-dashboard'),
]
