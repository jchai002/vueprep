from django.views.decorators.csrf import csrf_exempt
from vueprep.settings import PROJECT_ENV

def csrf_exempt_except_prod(function):
    if PROJECT_ENV('ENV') == 'prod':
        return function
    return csrf_exempt(function)
