import json
from django.db import models
from django.contrib.auth.models import User
from vueprep.settings import ERROR_STRING, STATUS, QUESTIONS_PER_INTERVIEW
from api.ai.interpret import avg_json, get_interpretation

class Gradable():
    def set_grade(self):
        if self.score == ERROR_STRING:
            self.grade = ERROR_STRING
        else:
            self.grade = \
            'A+' if float(self.score) > 90 else\
            'A'  if float(self.score) > 75 else \
            'A-' if float(self.score) > 60 else \
            'B+' if float(self.score) > 45 else \
            'B'  if float(self.score) > 30 else \
            'B-'
        return

class Industry(models.Model):
    name = models.TextField(unique=True, null=True)
    class Meta:
        ordering = ['-name']

    def __str__(self):
        return self.name

class Criterion(models.Model):
    name = models.TextField(unique=True, null=True)
    class Meta:
        ordering = ['-name']

    def __str__(self):
        return self.name

class Question(models.Model):
    content = models.TextField(unique=True, null=True)
    answer = models.TextField(unique=False, null=True)
    hint = models.TextField(unique=False, null=True)
    difficulty = models.IntegerField(unique=False, null=True)
    industries = models.ManyToManyField(Industry)
    criteria = models.ManyToManyField(Criterion)

    @classmethod
    def create_question_set(cls, criterion):
        limit = QUESTIONS_PER_INTERVIEW
        questions = cls.objects.filter(criteria__in=[criterion]).order_by('?').values()
        res = []
        for q in questions:
            res.append(q)
            limit -= 1
            if limit <= 0:
                break
        return json.dumps(res)

    @classmethod
    def get_valid_question_count(cls, criterion):
        valid_count = cls.objects.filter(criteria__in=[criterion]).count()
        return QUESTIONS_PER_INTERVIEW if QUESTIONS_PER_INTERVIEW < valid_count else valid_count

    class Meta:
        ordering = ['-content']

    def __str__(self):
        return self.content

class Interview(models.Model, Gradable):
    user = models.ForeignKey(User, on_delete= models.CASCADE, null=True)
    status = models.IntegerField(unique=False, default=STATUS['CREATED'])
    valid_question_count = models.IntegerField(unique=False, null=True)
    questions = models.ManyToManyField(Question, through='InterviewQuestion')
    score = models.TextField(unique=False, null=True)
    grade = models.CharField(max_length=10, unique=False, null=True)
    prediction = models.TextField(unique=False, null=True)
    interpretation = models.TextField(unique=False, null=True)

    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    def combine_results(self):
        interview_questions = InterviewQuestion.objects.filter(interview=self).all()
        total = 0
        i_q_count = 0
        predictions = []
        for i_q in interview_questions:
            if i_q.score and i_q.score != ERROR_STRING:
                i_q_count += 1
                total += float(i_q.score)
            if i_q.prediction != ERROR_STRING:
                predictions.append(i_q.prediction)
        if interview_questions.count() != 0:
            self.status = STATUS['COMPLETE']
            self.save()
        if total != 0:    
            self.score = str(round(total / i_q_count, 2))
            self.prediction = json.dumps(avg_json(predictions))
            self.interpretation = get_interpretation(self.prediction)
            self.set_grade()
            self.save()
            return True
        return False

    class Meta:
        ordering = ['-created']

    def __str__(self):
        return f'interview id: {self.id}'

class InterviewQuestion(models.Model, Gradable):
    interview = models.ForeignKey(Interview, on_delete= models.CASCADE)
    question = models.ForeignKey(Question, on_delete= models.CASCADE)
    video_url = models.TextField(unique=False, null=True)
    thumb_url = models.TextField(unique=False, null=True)
    score = models.TextField(unique=False, null=True)
    grade = models.CharField(max_length=10, unique=False, null=True)
    transcript = models.TextField(unique=False, null=True)
    prediction = models.TextField(unique=False, null=True)

    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    def set_data(self, prediction):
        self.prediction =  ERROR_STRING if 'error' in prediction else json.dumps(prediction)
        self.score = ERROR_STRING if 'error' in prediction else str(prediction['personality_traits']['overall'])
        self.set_grade()
        self.save()

    @classmethod
    def create_interview_question(cls, params, options):
        q = Question.objects.get(pk=int(params['q_id']))
        i = Interview.objects.get(pk=int(params['i_id']))
        # update interview status
        i.status = STATUS['IN_PROGRESS']
        i.save()
        # create interview question
        i_q = cls.objects.create(interview=i, question=q)
        i_q.video_url = params['data']['video_url']
        i_q.thumb_url = params['data']['thumb_url']
        if options['should_predict'] == True:
            i_q.prediction =  ERROR_STRING if 'error' in params['data']['prediction'] else json.dumps(params['data']['prediction'])
            i_q.score = ERROR_STRING if 'error' in params['data']['prediction'] else str(params['data']['prediction']['personality_traits']['overall'])
            i_q.set_grade()
        if options['should_transcribe'] == True:
            i_q.transcript = params['data']['transcript']
        i_q.save()
        return i_q

    @classmethod
    def get_interview_questions_by_i_id(cls, i_id):
        return cls.objects.filter(interview=i_id).order_by('created')
    
    @classmethod
    def get_interview_questions_with_personality_traits(cls, i_id):
        interview_questions = cls.objects.filter(interview=i_id).order_by('created')
        for i_q in interview_questions:
            if i_q.prediction and i_q.prediction != ERROR_STRING:
                personality_traits = json.loads(i_q.prediction)["personality_traits"]
                for t in personality_traits:
                    personality_traits[t] = str(personality_traits[t])
                i_q.personality_traits = personality_traits
        return interview_questions

    @classmethod
    def get_interview_question(cls, i_id, q_id):
        return cls.objects.filter(interview=int(i_id), question=int(q_id)).values()[0]

    class Meta:
        ordering = ['-created']

    def __str__(self):
        return self.question.content
