from django.shortcuts import render, redirect
from django.http import JsonResponse, QueryDict
from django.contrib.auth.decorators import login_required
from django.contrib import messages
from .models import Interview, Question, InterviewQuestion, Industry, Criterion
from vueprep.settings import ERROR_STRING, STATUS, QUESTIONS_PER_INTERVIEW
from .concerns.forms import UserRegisterForm
from .concerns.decorators import csrf_exempt_except_prod
from api.ai.interpret import avg_json

def home(request):
    return render(request, "main_app/home.html")

@csrf_exempt_except_prod
def mock_interview(request):
    if request.method == 'GET':
        q_count = QUESTIONS_PER_INTERVIEW if QUESTIONS_PER_INTERVIEW < Question.objects.count() else Question.objects.count()
        # TODO: add should_transcribe parameter
        return render(request, "main_app/mock_interview.html", { "run_prediction": False, "criteria": Criterion.objects.all(), "q_count": range(0, q_count) })
    if request.method == 'POST':
        if 'i_id' in request.POST:
            i = Interview.objects.get(pk=int(request.POST['i_id']))
            # handle partial interviews
            if i.questions.count() != 0 and i.combine_results():
                return JsonResponse({ 'score': i.score, 'grade': i.grade, 'interpretation': i.interpretation })
            return JsonResponse({ 'msg': 'interview processed' })
        else:
            if 'crit_id' in request.POST:
                criterion = Criterion.objects.get(pk=int(request.POST['crit_id']))
                questions = Question.create_question_set(criterion)
                i = Interview.objects.create(valid_question_count = Question.get_valid_question_count(criterion))
                if request.user.is_authenticated:
                    i.user = request.user
                    i.save()
            return JsonResponse({ 'i_id': i.id, 'questions': questions })

@csrf_exempt_except_prod
def interview_question(request):
    if 'i_id' and 'q_id' in request.GET:
        return JsonResponse({ 'q_id': request.GET['q_id'], 'interview_question': InterviewQuestion.get_interview_question(request.GET['i_id'], request.GET['q_id']) })
    return JsonResponse({ 'error': 'invalid request' })
    
def register(request):
    if request.method == 'POST':
        form = UserRegisterForm(request.POST)
        if form.is_valid():
            user = form.save()
            if 'i_id' in request.POST:
                i = Interview.objects.get(pk=int(request.POST['i_id']))
                i.user = user
                i.save()
            username = form.cleaned_data.get('username')
            messages.success(request, f'account created for {username}')
            return redirect('login')
    else:
        form = UserRegisterForm()
    return render(request, "main_app/register.html", { 'form': form })

@login_required
def dashboard(request):
    complete_interviews = []
    interviews = Interview.objects.filter(user=request.user, status=STATUS['COMPLETE']).values()
    for i in interviews:
        i['interview_questions'] =  InterviewQuestion.get_interview_questions_by_i_id(i['id'])
        complete_interviews.append(i)
    return render(request, "main_app/dashboard.html", { "interviews" : complete_interviews })

@login_required
def interview_details(request, id):
    i = Interview.objects.filter(id=id).values()[0]
    i['interview_questions'] =  InterviewQuestion.get_interview_questions_with_personality_traits(id)
    return render(request, "main_app/interview_details.html", { "interview": i })