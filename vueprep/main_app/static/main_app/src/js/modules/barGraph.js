export function setBarGraph($wrapper, data) {
  // Get Max Value From Array
  var traits = Object.keys(data);
  var values = Object.values(data);
  // Set width of bar to percent of max value
  $wrapper.find(".bar-row").each((i, el) => {
    let value = +values[i];
    let $barRow = $(el);
    let $bar = $barRow.find(".bar");
    // Set Width & Add Class to show
    $bar.width(value + "%");
    $barRow.find(".label").text(traits[i]);
    $barRow.find(".number").text(value);
  });
}

export function animateBarGraph($wrapper) {
  $wrapper.find(".bar-row").each((i, el) => {
    let $barRow = $(el);
    $barRow.find(".bar").addClass("in");
  });
}

// export function displayResults() {
//   console.log("displaying results..");
//   $("#btnUpload").prop("disabled", false);
//   $("#spinner").hide();
//   $(".result").show();
//   // var resJson = JSON.parse(res);
//   var resJson = {
//     "recording quality": {
//       loudness: "good",
//       speech: "not detected",
//       facial: "good",
//       occlusion: "yes",
//       blurness: "blurry",
//       lighting: "good",
//     },
//     "facial attributes": {
//       gender: "male",
//       age: 29,
//       beauty: 56,
//       smile: 3,
//       emotion: "neutral",
//       eyegaze: "not center",
//     },
//     "personality traits": {
//       interview: 64,
//       openness: 68,
//       conscientiousness: 64,
//       extraversion: 64,
//       agreeableness: 66,
//       neuroticism: 65,
//     },
//   };
//   // resJson = JSON.parse(resJson);

//   console.log(resJson);
//   // const facial = resJson["facial attributes"];
//   //
//   // for (const trait in facial) {
//   //   console.log(`${trait}: ${facial[trait]}`);
//   //   $("#facial-result").append(`<p>${trait}: ${facial[trait]}</p>`);
//   // }

//   var traits = [];
//   var values = [];
//   const personality = resJson["personality traits"];
//   for (const trait in personality) {
//     console.log(`${trait}: ${personality[trait]}`);
//     traits.push(trait);
//     values.push(personality[trait]);
//   }
//   generateBarGraph("#personality", traits, values);

//   // traits = [];
//   // values = [];
//   // const social = resJson["social evaluations"];
//   // for (const trait in social) {
//   //   console.log(`${trait}: ${social[trait]}`);
//   //   traits.push(trait);
//   //   values.push(social[trait]);
//   // }
//   // generateBarGraph("#social", traits, values);
//   // $(".cta-1").css("display", "block");
// }
