export async function webcamDriver(selector) {
  let constraintObj = {
    audio: true,
    video: {
      facingMode: "user",
      width: { min: 640, ideal: 1280, max: 1920 },
      height: { min: 480, ideal: 720, max: 1080 },
    },
  };
  // width: 1280, height: 720  -- preference only
  // facingMode: {exact: "user"}
  // facingMode: "environment"

  let video = document.querySelector(selector);

  //handle older browsers that might implement getUserMedia in some way
  if (navigator.mediaDevices === undefined) {
    navigator.mediaDevices = {};
    navigator.mediaDevices.getUserMedia = function (constraintObj) {
      let getUserMedia =
        navigator.webkitGetUserMedia || navigator.mozGetUserMedia;
      if (!getUserMedia) {
        alert(
          "webcam access is not supported in this browser, plase try to use chrome or firefox!"
        );
        return Promise.reject(
          new Error("getUserMedia is not implemented in this browser")
        );
      }
      return new Promise(function (resolve, reject) {
        getUserMedia.call(navigator, constraintObj, resolve, reject);
      });
    };
  } else {
    navigator.mediaDevices
      .enumerateDevices()
      .then((devices) => {
        devices.forEach((device) => {
          // console.log(device.kind.toUpperCase(), device.label);
          //, device.deviceId
        });
      })
      .catch((err) => {
        alert(
          "webcam access is not supported in this browser, plase try to use chrome or firefox!"
        );
        console.log(err.name, err.message);
      });
  }

  let mediaStreamObj = await navigator.mediaDevices.getUserMedia(constraintObj);
  let mediaRecorder = await new MediaRecorder(mediaStreamObj);

  if ("srcObject" in video) {
    video.srcObject = mediaStreamObj;
  } else {
    //old version
    video.src = window.URL.createObjectURL(mediaStreamObj);
  }

  video.onloadedmetadata = (ev) => {
    //show in the video element what is being captured by the webcam
    video.play();
  };

  return mediaRecorder;
}
