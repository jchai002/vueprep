export function setGrade($grade, score, grade) {
  $grade.text(grade);
  if (Number(score) > 60) {
    $grade.addClass("text-success");
  } else {
    $grade.addClass("text-warning");
  }
}
