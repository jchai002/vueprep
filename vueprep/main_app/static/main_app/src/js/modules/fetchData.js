export function fetchData(url, body) {
  return fetch(url, {
    method: "POST",
    mode: "cors",
    headers: { "X-CSRFToken": $("input[name='csrfmiddlewaretoken']").val() },
    body,
  });
}
