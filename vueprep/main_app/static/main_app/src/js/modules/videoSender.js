import { getThumbnails } from "video-metadata-thumbnails";
import { ERROR_STRING, BASE_URL } from "../constants";
import { fetchData } from "./fetchData";
import { setBarGraph, animateBarGraph } from "./barGraph";
import { setGrade } from "./setGrade";

export default class VideoSender {
  constructor(
    q_count,
    i_id,
    should_predict = false,
    should_transcribe = false
  ) {
    this.q_count = q_count;
    this.r_count = 0;
    this.i_id = i_id;
    this.should_predict = false;
    this.should_transcribe = should_transcribe;
  }

  async sendVideo(blob, q) {
    if (!this.i_id) {
      console.error("cannot find current interview");
      return;
    }
    if (!q) {
      console.error("cannot find current question");
      return;
    }
    var q_content = q.content
      .replace(/[^\w\s]/gi, "")
      .replace(/ /g, "_")
      .toLowerCase();
    var thumbnails;
    var u_id = $("#u_id").val();
    var videoName = `browser_recorded_${q_content}.webm`;
    var thumbName = `thumb_${q_content}.jpg`;

    // make form data
    var formData = new FormData();
    formData.append("video", blob, videoName);
    formData.append("q_id", q.id);
    formData.append("i_id", this.i_id);

    try {
      thumbnails = await getThumbnails(blob, {
        quality: 0.8,
        scale: 0.5,
        start: 1,
        end: 1,
      });
      formData.append("thumb", thumbnails[0].blob, thumbName);
    } catch (e) {
      console.error(e);
      this.handleError(q);
      return;
    }

    var postRes, getRes;
    try {
      console.log("sending video...", this.i_id);
      if (this.should_predict) {
        formData.append("should_predict", true);
      }
      postRes = await fetchData(
        `${BASE_URL}/api/process-interview-question`,
        formData
      );

      if (postRes && postRes.error) {
        this.handleError(q);
        return;
      }
    } catch (e) {
      console.error("post res errored:", e);
      this.handleError(q);
      return;
    }

    let url = new URL(`${BASE_URL}/interview-question`);
    url.search = new URLSearchParams({
      i_id: this.i_id,
      q_id: q.id,
    }).toString();
    try {
      getRes = await fetch(url);
      getRes = await getRes.json();
      console.log(getRes);
      if (getRes && getRes.error) {
        console.error("get res errored", getRes.error);
        this.handleError(q);
        return;
      }
      this.setResult(getRes, q);
    } catch (e) {
      console.error("get res errored", e);
      this.handleError(q);
      return;
    }

    // if function didn't return here, question requests success
    this.r_count++;
    console.log("this.r_count", this.r_count);
    if (this.r_count == this.q_count) this.setFinalResult();
  }

  handleError(q) {
    var $resultWrapper = $("#result-" + q.id);
    $resultWrapper.find(".placeholder").hide();
    $resultWrapper.find(".error").show();
    this.r_count++;
    console.log("this.r_count", this.r_count);
  }

  setResult(res, q) {
    console.log(res);
    if (res && res.interview_question) {
      var $resultWrapper = $("#result-" + res.q_id);
      var $resultContent = $resultWrapper.find(".card-content");

      $resultContent.show();
      $resultContent
        .find("video")
        .attr("src", res.interview_question.video_url);
      $resultWrapper.find(".placeholder").hide();
      if (this.should_predict) {
        if (res.interview_question.score !== ERROR_STRING) {
          setBarGraph(
            $resultContent.find(".chart"),
            JSON.parse(res.interview_question.prediction).personality_traits
          );
          animateBarGraph($resultContent.find(".chart"));
          setGrade(
            $resultContent.find(".score"),
            res.interview_question.score,
            res.interview_question.grade
          );
          $resultWrapper.find(".score-wrapper").show();
        } else {
          $resultContent.find(".error-analyze").show();
        }
      }
    } else {
      $("#result-" + q.id)
        .find(".error")
        .show();
    }
  }

  async finishInterview() {
    var formData = new FormData();
    formData.append("i_id", this.i_id);
    return await fetchData(`${BASE_URL}/mock-interview`, formData);
  }

  async setFinalResult() {
    var $grade = $("#final-score");
    var res;

    try {
      res = await this.finishInterview();
      res = await res.json();
      console.log(res);
      if (this.should_predict) {
        if (res.error || res.grade == ERROR_STRING) {
          $grade.text(ERROR_STRING).addClass("text-danger");
        } else {
          setGrade($grade, res.score, res.grade);
          $(".feedback-wrapper").show();
          $(".feedback-content").text(res.interpretation);
        }
      }
    } catch (e) {
      console.error(e);
      $grade.text("ERROR").addClass("text-danger");
    }
  }
}
