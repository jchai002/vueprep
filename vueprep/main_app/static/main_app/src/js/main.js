import { mockInterview } from "./pages/mockInterview";
import { register } from "./pages/register";
import { home } from "./pages/home";
import { interviewDetails } from "./pages/interviewDetials";

$(document).ready(function () {
  // page functions
  if ($("#mock-interview")[0]) {
    mockInterview();
  }

  if ($("#interview-details")[0]) {
    interviewDetails();
  }

  if ($("#register")[0]) {
    register();
  }

  if ($("#home")[0]) {
    home();
  }

  // $("#videoFile").on("change", function() {
  //   //get the file name
  //   var fileName = $(this)
  //     .val()
  //     .replace(/^.*\\/, "");
  //   //replace the "Choose a file" label
  //   $(this)
  //     .next(".custom-file-label")
  //     .html(fileName);
  // });
  //
  // $("input:file").on("change", function() {
  //   if ($(this).val()) {
  //     $("input:submit").attr("disabled", false);
  //   }
  // });
  //
  // $("#background-mode input").on("change", function() {
  //   $("body").addClass(".body");
  //   alert($("input[name=radioName]:checked", "#background-mode").val());
  // });
  //
  // $("input[type='button']").click(function() {
  //   var radioValue = $("input[name='gender']:checked").val();
  //   if (radioValue) {
  //     alert("Your are a - " + radioValue);
  //   }
  // });
  //
  // $("#btnUpload").click(async ev => {
  //   //stop submit the form, we will post it manually.
  //   ev.preventDefault();
  //
  //   $(".UploadForm").hide();
  //
  //   // Create an FormData object
  //   var fileInput = document.getElementById("videoFile");
  //   var file = fileInput.files[0];
  //   var formData = new FormData();
  //   formData.append("file", file);
  //   console.log("formData", formData.entries());
  //
  //   // disabled the submit button
  //   $("#btnUpload").prop("disabled", true);
  //   $("#spinner").show();
  //   $(".result").hide();
  //   let res;
  //   try {
  //     res = await sendVideo(formData);
  //   } catch (err) {
  //     throw err;
  //   }
  //   displayResults(res);
  // });
});
