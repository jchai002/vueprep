export const ERROR_STRING = "ERRORED";
export const BASE_URL =
  globalThis.process && globalThis.process.env.NODE_ENV === "production"
    ? "https://vueprep.com"
    : "http://localhost:8000";
