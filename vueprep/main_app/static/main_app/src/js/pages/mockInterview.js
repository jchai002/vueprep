import { webcamDriver } from "../modules/webcamDriver";
import VideoSender from "../modules/videoSender";
import { BASE_URL } from "../constants";
import { fetchData } from "../modules/fetchData";

export async function mockInterview() {
  // setup
  var questions;
  var currentQuestion;
  var videoSender;
  var mediaRecorder;
  var timerInterval = null;
  var timeSecPerQuestion = 60;
  var breakSecPerQuestion = 15;
  var $questionDisplay = $("#question");
  var $timeDisplay = $("#timer");
  var $videoDisplay = $(".video-wrapper");
  var $btnBegin = $("#btn-begin");
  var $btnRecord = $("#btn-record");
  var $btnNext = $("#btn-next");
  var $btnResults = $("#btn-results");
  var $notice = $("#notice");
  var $readyGO = $("#ready-go")

  // functions
  function init() {
    $("#video-switch").on("change", () => {
      $("#video-recorder").toggleClass("transparent");
    });
    $(".selections .custom-select").on("change", () => {
      if ($("#criterion-select").val() != null)
        $btnBegin.prop("disabled", false);
    });
  }

  async function setupWebcam() {
    var chunks = [];
    mediaRecorder = await webcamDriver("#video-recorder");
    mediaRecorder.ondataavailable = (ev) => {
      chunks.push(ev.data);
    };
    mediaRecorder.onstop = (ev) => {
      try {
        var blob = new Blob(chunks, { type: "video/webm" });
        videoSender.sendVideo(blob, currentQuestion);
      } catch (e) {
        console.error(e);
      } finally {
        chunks = [];
        startBreak(breakSecPerQuestion);
      }
    };

    document.addEventListener("onbeforeunload", () => {
      videoSender.finishInterview();
    });
  }

  function setupResultCards() {
    $(".card").each((i, card) => {
      let $card = $(card);
      if (questions[i]) {
        $card.attr("id", "result-" + questions[i].id);
        $card.find(".card-title").text(questions[i].content);
      } else {
        $card.remove();
      }
    });
  }

  async function beginInterview() {
    $questionDisplay.text("Session starting");
    // create interview in
    var formData = new FormData();
    formData.append("crit_id", $("#criterion-select").val());
    var res = await fetchData(`${BASE_URL}/mock-interview`, formData);
    res = await res.json();
    questions = JSON.parse(res.questions);
    var minutes = $("#minute-select").val();
    timeSecPerQuestion *= minutes;
    console.log(minutes, timeSecPerQuestion);
    if (!questions.length) {
      $questionDisplay.html(
        "<p>We don't have any questions matching your filters... </p> <p>Please try something else!</p>"
      );
      return;
    }

    setupResultCards();
    // init video sender
    videoSender = new VideoSender(questions.length, res.i_id);
    // start webcam
    await setupWebcam();

    // update register link
    if ($("#interview-register")[0]) {
      let $btnReg = $("#interview-register");
      $btnReg.attr("href", $btnReg.attr("href") + `?i_id=${res.i_id}`);
    }

    // start a quick countdown
    var count = 3;
    while (count !== 0) {
      $questionDisplay.append(".");
      await sleep(1);
      count--;
      if (count === 0) {
        // show first question and start break timer
        startBreak(breakSecPerQuestion);
        $videoDisplay.fadeIn();
      }
    }
  }

  function sleep(s) {
    return new Promise((resolve) => setTimeout(resolve, s * 1000));
  }

  function setNextQuestion() {
    if (questions.length) {
      currentQuestion = questions.shift();
      $questionDisplay.text(currentQuestion.content);
    }
  }

  function setTimerValue(duration) {
    var minutes = parseInt(duration / 60, 10);
    var seconds = parseInt(duration % 60, 10);
    minutes = minutes < 10 ? "0" + minutes : minutes;
    seconds = seconds < 10 ? "0" + seconds : seconds;
    $timeDisplay.text(minutes + ":" + seconds);
  }

  function startBreak(duration) {
    if (questions.length) {
      setNextQuestion();
    } else {
      return;
    }
    $notice.show();
    $btnNext.hide();
    $btnRecord.show();
    $notice.text("Take some time to read the question above.");
    setTimerValue(duration);
    timerInterval = setInterval(async () => {
      duration--;
      setTimerValue(duration);
      if (duration < 5) {
        $notice.text(
          "Get ready! Your response starts in " + (duration + 1) + "."
        );
      }
      if (duration === 0) {
        await sleep(1);
        stopBreakAndStartCountDown();
      }
    }, 1000);
  }

  function stopBreakAndStartCountDown() {
    clearInterval(timerInterval);
    $btnRecord.hide();
    $notice.hide();
    startCountDown(timeSecPerQuestion);
    $readyGO.show();
    setTimeout(function() {
      $readyGO.fadeOut('slow','linear');
    }, 400);

    if (questions.length) {
      $btnNext.show();
    } else {
      $btnResults.show();
    }
  }

  function startRecorder() {
    if (mediaRecorder.state !== "recording") {
      $(".circle.blinking").show();
      mediaRecorder.start();
    }
  }

  async function stopRecorder() {
    if (mediaRecorder.state === "recording") {
      $(".circle.blinking").hide();
      await mediaRecorder.stop();
    }
  }
  async function startCountDown(duration) {
    startRecorder();
    setTimerValue(duration);
    timerInterval = await setInterval(async () => {
      duration--;
      setTimerValue(duration);
      if (duration === 0) {
        await sleep(1);
        await stopCountDown();
      }
    }, 1000);
  }

  async function stopCountDown() {
    setTimerValue(0);
    await stopRecorder();
    clearInterval(timerInterval);
  }

  // register events
  $btnBegin.on("click", async () => {
    $btnBegin.hide();
    $(".selections").hide();
    beginInterview();
  });

  $btnRecord.on("click", () => {
    stopBreakAndStartCountDown();
  });

  $btnNext.on("click", async () => {
    await stopCountDown();
  });

  $btnResults.on("click", async () => {
    await stopCountDown();
    $videoDisplay.fadeOut();
    $questionDisplay.fadeOut();
    $(".results-wrapper").fadeIn();
  });

  init();
}
