import { BASE_URL, ERROR_STRING } from "../constants";
import { setBarGraph, animateBarGraph } from "../modules/barGraph";
import { fetchData } from "../modules/fetchData";
import { setGrade } from "../modules/setGrade";

export function interviewDetails() {
  $(".btn-predict-question").on("click", async (e) => {
    var $this = $(e.target);
    var i_q_id = $this.data("id");
    var formData = new FormData();
    var $resultsWrapper = $("#interview-question-" + i_q_id).find(
      ".results-wrapper"
    );
    var $loader = $resultsWrapper.find(".loader");
    var $status = $resultsWrapper.find(".status");
    var $grade = $resultsWrapper.find(".grade");

    // after click
    $loader.show();
    $status.text("Analyzing...");
    $this.hide();
    // make prediction
    formData.append("i_q_id", i_q_id);
    var res = await fetchData(
      `${BASE_URL}/api/predict-interview-question`,
      formData
    );
    res = await res.json();
    console.log(res);
    // after result
    if (res) {
      $status.hide();
      $loader.hide();
    }

    var { interview_question, interview } = res;
    if (interview_question.error || interview_question.grade == ERROR_STRING) {
      $resultsWrapper.find(".video-error").show();
    } else {
      setGrade($grade, interview_question.score, interview_question.grade);
      setBarGraph(
        $(`#modal-${interview_question.id} .chart`),
        JSON.parse(interview_question.prediction).personality_traits
      );
      $resultsWrapper.find(".result").show();
      $resultsWrapper.find(".modal-launch").show();
    }

    if (!interview.error && interview.grade != ERROR_STRING) {
      var $wrapper = $(".interview-grade");
      setGrade(
        $wrapper.find(".grade"),
        interview.score,
        interview_question.grade
      );
      $wrapper.show();
    }

    if (!interview.error && interview.interpretation != ERROR_STRING) {
      $(".interview-interpretation-wrapper").show();
      $(".interview-interpretation").text(interview.interpretation);
    }
  });

  $(".modal-launch").on("click", (e) => {
    var $this = $(e.target);
    var i_q_id = $this.data("id");
    animateBarGraph($(`#modal-${i_q_id} .chart`));
  });

  $(".generate-transcript").on("click", async (e) => {
    var $this = $(e.target);
    var i_q_id = $this.data("id");
    var $wrapper = $(`.transcript#transcript-${i_q_id}`);
    $this.hide();
    $wrapper.find(".loader").show();
    $wrapper.find(".status").text("Generating...");
    let res;
    try {
      let formData = new FormData();
      formData.append("i_q_id", i_q_id);
      res = await fetchData(
        `${BASE_URL}/api/transcribe-interview-question`,
        formData
      );
      res = await res.json();
      $wrapper.html(`<p>${res.transcript}</p>`);
    } catch (e) {
      console.error(e);
    }
  });
}
