export function home() {
  function init() {
    $(".on-view").waypoint(
      function () {
        $(this.element).css("opacity", 1);
        $(this.element).addClass("animate__fadeInUp");
      },
      { offset: "100%" }
    );
  }

  init();
}
