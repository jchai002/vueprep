(function (factory) {
    typeof define === 'function' && define.amd ? define('main', factory) :
    factory();
}((function () { 'use strict';

    (function() {
        const env = {};
        try {
            if (process) {
                process.env = Object.assign({}, process.env);
                Object.assign(process.env, env);
                return;
            }
        } catch (e) {} // avoid ReferenceError: process is not defined
        globalThis.process = { env:env };
    })();

    async function webcamDriver(selector) {
      let constraintObj = {
        audio: true,
        video: {
          facingMode: "user",
          width: {
            min: 640,
            ideal: 1280,
            max: 1920
          },
          height: {
            min: 480,
            ideal: 720,
            max: 1080
          }
        }
      }; // width: 1280, height: 720  -- preference only
      // facingMode: {exact: "user"}
      // facingMode: "environment"

      let video = document.querySelector(selector); //handle older browsers that might implement getUserMedia in some way

      if (navigator.mediaDevices === undefined) {
        navigator.mediaDevices = {};

        navigator.mediaDevices.getUserMedia = function (constraintObj) {
          let getUserMedia = navigator.webkitGetUserMedia || navigator.mozGetUserMedia;

          if (!getUserMedia) {
            alert("webcam access is not supported in this browser, plase try to use chrome or firefox!");
            return Promise.reject(new Error("getUserMedia is not implemented in this browser"));
          }

          return new Promise(function (resolve, reject) {
            getUserMedia.call(navigator, constraintObj, resolve, reject);
          });
        };
      } else {
        navigator.mediaDevices.enumerateDevices().then(devices => {
          devices.forEach(device => {// console.log(device.kind.toUpperCase(), device.label);
            //, device.deviceId
          });
        }).catch(err => {
          alert("webcam access is not supported in this browser, plase try to use chrome or firefox!");
          console.log(err.name, err.message);
        });
      }

      let mediaStreamObj = await navigator.mediaDevices.getUserMedia(constraintObj);
      let mediaRecorder = await new MediaRecorder(mediaStreamObj);

      if ("srcObject" in video) {
        video.srcObject = mediaStreamObj;
      } else {
        //old version
        video.src = window.URL.createObjectURL(mediaStreamObj);
      }

      video.onloadedmetadata = ev => {
        //show in the video element what is being captured by the webcam
        video.play();
      };

      return mediaRecorder;
    }

    /*!
       * Video metadata and thumbnails v1.0.18
       *
       * @author wangweiwei
       */
    function e(e) {
      var t = this.constructor;
      return this.then(function (n) {
        return t.resolve(e()).then(function () {
          return n;
        });
      }, function (n) {
        return t.resolve(e()).then(function () {
          return t.reject(n);
        });
      });
    }

    var t = setTimeout;

    function n(e) {
      return Boolean(e && void 0 !== e.length);
    }

    function r() {}

    function o(e) {
      if (!(this instanceof o)) throw new TypeError("Promises must be constructed via new");
      if ("function" != typeof e) throw new TypeError("not a function");
      this._state = 0, this._handled = !1, this._value = void 0, this._deferreds = [], s(e, this);
    }

    function i(e, t) {
      for (; 3 === e._state;) e = e._value;

      0 !== e._state ? (e._handled = !0, o._immediateFn(function () {
        var n = 1 === e._state ? t.onFulfilled : t.onRejected;

        if (null !== n) {
          var r;

          try {
            r = n(e._value);
          } catch (e) {
            return void u(t.promise, e);
          }

          a(t.promise, r);
        } else (1 === e._state ? a : u)(t.promise, e._value);
      })) : e._deferreds.push(t);
    }

    function a(e, t) {
      try {
        if (t === e) throw new TypeError("A promise cannot be resolved with itself.");

        if (t && ("object" == typeof t || "function" == typeof t)) {
          var n = t.then;
          if (t instanceof o) return e._state = 3, e._value = t, void l(e);
          if ("function" == typeof n) return void s((r = n, i = t, function () {
            r.apply(i, arguments);
          }), e);
        }

        e._state = 1, e._value = t, l(e);
      } catch (t) {
        u(e, t);
      }

      var r, i;
    }

    function u(e, t) {
      e._state = 2, e._value = t, l(e);
    }

    function l(e) {
      2 === e._state && 0 === e._deferreds.length && o._immediateFn(function () {
        e._handled || o._unhandledRejectionFn(e._value);
      });

      for (var t = 0, n = e._deferreds.length; t < n; t++) i(e, e._deferreds[t]);

      e._deferreds = null;
    }

    function c(e, t, n) {
      this.onFulfilled = "function" == typeof e ? e : null, this.onRejected = "function" == typeof t ? t : null, this.promise = n;
    }

    function s(e, t) {
      var n = !1;

      try {
        e(function (e) {
          n || (n = !0, a(t, e));
        }, function (e) {
          n || (n = !0, u(t, e));
        });
      } catch (e) {
        if (n) return;
        n = !0, u(t, e);
      }
    }

    o.prototype.catch = function (e) {
      return this.then(null, e);
    }, o.prototype.then = function (e, t) {
      var n = new this.constructor(r);
      return i(this, new c(e, t, n)), n;
    }, o.prototype.finally = e, o.all = function (e) {
      return new o(function (t, r) {
        if (!n(e)) return r(new TypeError("Promise.all accepts an array"));
        var o = Array.prototype.slice.call(e);
        if (0 === o.length) return t([]);
        var i = o.length;

        function a(e, n) {
          try {
            if (n && ("object" == typeof n || "function" == typeof n)) {
              var u = n.then;
              if ("function" == typeof u) return void u.call(n, function (t) {
                a(e, t);
              }, r);
            }

            o[e] = n, 0 == --i && t(o);
          } catch (e) {
            r(e);
          }
        }

        for (var u = 0; u < o.length; u++) a(u, o[u]);
      });
    }, o.resolve = function (e) {
      return e && "object" == typeof e && e.constructor === o ? e : new o(function (t) {
        t(e);
      });
    }, o.reject = function (e) {
      return new o(function (t, n) {
        n(e);
      });
    }, o.race = function (e) {
      return new o(function (t, r) {
        if (!n(e)) return r(new TypeError("Promise.race accepts an array"));

        for (var i = 0, a = e.length; i < a; i++) o.resolve(e[i]).then(t, r);
      });
    }, o._immediateFn = "function" == typeof setImmediate && function (e) {
      setImmediate(e);
    } || function (e) {
      t(e, 0);
    }, o._unhandledRejectionFn = function (e) {
      "undefined" != typeof console && console && console.warn("Possible Unhandled Promise Rejection:", e);
    };

    var d = function () {
      if ("undefined" != typeof self) return self;
      if ("undefined" != typeof window) return window;
      if ("undefined" != typeof global) return global;
      throw new Error("unable to locate global object");
    }();
    /*! *****************************************************************************
    Copyright (c) Microsoft Corporation. All rights reserved.
    Licensed under the Apache License, Version 2.0 (the "License"); you may not use
    this file except in compliance with the License. You may obtain a copy of the
    License at http://www.apache.org/licenses/LICENSE-2.0

    THIS CODE IS PROVIDED ON AN *AS IS* BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
    KIND, EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION ANY IMPLIED
    WARRANTIES OR CONDITIONS OF TITLE, FITNESS FOR A PARTICULAR PURPOSE,
    MERCHANTABLITY OR NON-INFRINGEMENT.

    See the Apache Version 2.0 License for specific language governing permissions
    and limitations under the License.
    ***************************************************************************** */


    function v(e, t, n, r) {
      return new (n || (n = Promise))(function (o, i) {
        function a(e) {
          try {
            l(r.next(e));
          } catch (e) {
            i(e);
          }
        }

        function u(e) {
          try {
            l(r.throw(e));
          } catch (e) {
            i(e);
          }
        }

        function l(e) {
          e.done ? o(e.value) : new n(function (t) {
            t(e.value);
          }).then(a, u);
        }

        l((r = r.apply(e, t || [])).next());
      });
    }

    function f(e, t) {
      var n,
          r,
          o,
          i,
          a = {
        label: 0,
        sent: function () {
          if (1 & o[0]) throw o[1];
          return o[1];
        },
        trys: [],
        ops: []
      };
      return i = {
        next: u(0),
        throw: u(1),
        return: u(2)
      }, "function" == typeof Symbol && (i[Symbol.iterator] = function () {
        return this;
      }), i;

      function u(i) {
        return function (u) {
          return function (i) {
            if (n) throw new TypeError("Generator is already executing.");

            for (; a;) try {
              if (n = 1, r && (o = 2 & i[0] ? r.return : i[0] ? r.throw || ((o = r.return) && o.call(r), 0) : r.next) && !(o = o.call(r, i[1])).done) return o;

              switch (r = 0, o && (i = [2 & i[0], o.value]), i[0]) {
                case 0:
                case 1:
                  o = i;
                  break;

                case 4:
                  return a.label++, {
                    value: i[1],
                    done: !1
                  };

                case 5:
                  a.label++, r = i[1], i = [0];
                  continue;

                case 7:
                  i = a.ops.pop(), a.trys.pop();
                  continue;

                default:
                  if (!(o = (o = a.trys).length > 0 && o[o.length - 1]) && (6 === i[0] || 2 === i[0])) {
                    a = 0;
                    continue;
                  }

                  if (3 === i[0] && (!o || i[1] > o[0] && i[1] < o[3])) {
                    a.label = i[1];
                    break;
                  }

                  if (6 === i[0] && a.label < o[1]) {
                    a.label = o[1], o = i;
                    break;
                  }

                  if (o && a.label < o[2]) {
                    a.label = o[2], a.ops.push(i);
                    break;
                  }

                  o[2] && a.ops.pop(), a.trys.pop();
                  continue;
              }

              i = t.call(e, a);
            } catch (e) {
              i = [6, e], r = 0;
            } finally {
              n = o = 0;
            }

            if (5 & i[0]) throw i[1];
            return {
              value: i[0] ? i[1] : void 0,
              done: !0
            };
          }([i, u]);
        };
      }
    }

    "Promise" in d ? d.Promise.prototype.finally || (d.Promise.prototype.finally = e) : d.Promise = o, "function" != typeof Object.assign && Object.defineProperty(Object, "assign", {
      value: function (e, t) {
        if (null == e) throw new TypeError("Cannot convert undefined or null to object");
        let n = Object(e);

        for (var r = 1; r < arguments.length; r++) {
          var o = arguments[r];
          if (null != o) for (let e in o) Object.prototype.hasOwnProperty.call(o, e) && (n[e] = o[e]);
        }

        return n;
      },
      writable: !0,
      configurable: !0
    });

    var m = function () {
      function e(e) {
        var t = this;
        if (this.thumbnails = [], this.option = {
          quality: .7,
          interval: 1,
          scale: .7,
          start: 0
        }, this.isStarted = !0, this.count = 0, this.version = "1.0.18", !e) throw new Error("video-metadata-thumbnails params error");
        this.videoElement = document.createElement("video"), this.videoElement.preload = "metadata", this.videoElement.muted = !0, this.videoElement.volume = 0;
        var n = document.createElement("canvas");
        this.canvas = n, this.canvasContext = n.getContext("2d");
        var r = window.URL || window.webkitURL;
        this.videoElement.src = "string" == typeof e || e instanceof String ? e : r.createObjectURL(e);

        var o = function () {
          var e = t.videoElement.src;
          "string" == typeof e || e instanceof String || r.revokeObjectURL(t.videoElement.src), t.videoElement.removeEventListener("ended", o, !1);
        };

        this.videoElement.addEventListener("ended", o, !1);
      }

      return e.prototype.getVersion = function () {
        return this.version;
      }, e.prototype.getThumbnails = function (e) {
        var t = this;
        return e && (this.option = Object.assign(this.option, e)), new Promise(function (e, n) {
          var r = function () {
            var e = t.option.interval || 1,
                n = t.videoElement;
            n.videoWidth, n.videoHeight, n.duration;
            t.isStarted ? (t.videoElement.currentTime = t.option.start, t.isStarted = !1) : t.videoElement.currentTime += e;
          },
              o = function () {
            var i = t,
                a = i.option,
                u = i.videoElement,
                l = i.canvasContext,
                c = i.thumbnails,
                s = u.videoWidth,
                d = u.videoHeight,
                v = u.duration,
                f = a.quality,
                m = a.interval,
                h = a.start,
                p = a.end,
                E = a.scale,
                y = u.currentTime,
                w = y >= v || y > (void 0 === p ? v : p),
                b = m || 1,
                g = s * (E || 1),
                L = d * (E || 1);

            try {
              if (t.canvas.width = g, t.canvas.height = L, t.canvasContext.drawImage(t.videoElement, 0, 0, g, L), t.canvas.toBlob) t.canvas.toBlob(function (n) {
                if (l.clearRect(0, 0, g, L), l.restore(), w) return u.removeEventListener("canplaythrough", r, !1), u.removeEventListener("timeupdate", o, !1), void e(t.thumbnails);
                c.push({
                  currentTime: h + b * t.count,
                  blob: n
                }), t.count++;
              }, "image/jpeg", f);else {
                for (var _ = atob(t.canvas.toDataURL("image/jpeg", f).split(",")[1]), j = _.length, T = new Uint8Array(j), P = 0; P < j; P++) T[P] = _.charCodeAt(P);

                var R = new Blob([T], {
                  type: "image/jpeg"
                });
                if (l.clearRect(0, 0, g, L), l.restore(), w) return u.removeEventListener("canplaythrough", r, !1), u.removeEventListener("timeupdate", o, !1), void e(t.thumbnails);
                c.push({
                  currentTime: h + b * t.count,
                  blob: R
                }), t.count++;
              }
            } catch (e) {
              n(e);
            }
          },
              i = function () {
            t.videoElement.play(), t.videoElement.removeEventListener("progress", i, !1);
          };

          /^((?!chrome).)*safari((?!chrome).)*$/i.test(navigator.userAgent) && t.videoElement.addEventListener("progress", i, !1);

          var a = function () {
            t.videoElement.removeEventListener("progress", i, !1), t.videoElement.removeEventListener("ended", a, !1), t.videoElement.removeEventListener("canplaythrough", r, !1), t.videoElement.removeEventListener("timeupdate", o, !1);
          };

          t.videoElement.addEventListener("canplaythrough", r, !1), t.videoElement.addEventListener("timeupdate", o, !1), t.videoElement.addEventListener("ended", a, !1);
        });
      }, e.prototype.drawThumbnails = function () {}, e.prototype.getMetadata = function () {
        var e = this;
        return new Promise(function (t, n) {
          var r = function () {
            try {
              var o = e.videoElement,
                  i = o.videoWidth,
                  a = o.videoHeight,
                  u = o.duration;

              if (u === 1 / 0) {
                var l = function () {
                  e.videoElement.removeEventListener("timeupdate", l, !1), e.videoElement.removeEventListener("loadedmetadata", r, !1), t({
                    width: Math.floor(100 * e.videoElement.videoWidth) / 100,
                    height: Math.floor(100 * e.videoElement.videoHeight) / 100,
                    duration: Math.floor(100 * e.videoElement.duration) / 100
                  }), e.videoElement.currentTime = 0;
                };

                e.videoElement.addEventListener("timeupdate", l, !1), e.videoElement.currentTime = Number.MAX_SAFE_INTEGER;
              } else t({
                width: Math.floor(100 * i) / 100,
                height: Math.floor(100 * a) / 100,
                duration: Math.floor(100 * u) / 100
              }), e.videoElement.removeEventListener("loadedmetadata", r, !1);
            } catch (e) {
              n(e);
            }
          },
              o = function () {
            e.videoElement.removeEventListener("loadedmetadata", r, !1), e.videoElement.removeEventListener("ended", o, !1), e.videoElement.removeEventListener("error", i, !1);
          },
              i = function () {
            var t = e.videoElement.error;
            n(t ? new Error("video-metadata-thumbnails error " + t.code + "; details: " + t.message) : new Error("video-metadata-thumbnails unknown error")), e.videoElement.removeEventListener("loadedmetadata", r, !1), e.videoElement.removeEventListener("ended", o, !1), e.videoElement.removeEventListener("error", i, !1);
          };

          e.videoElement.addEventListener("loadedmetadata", r, !1), e.videoElement.addEventListener("ended", o, !1), e.videoElement.addEventListener("error", i, !1);
        });
      }, e;
    }();

    function p(e, t) {
      return v(this, void 0, void 0, function () {
        return f(this, function (n) {
          switch (n.label) {
            case 0:
              return [4, new m(e).getThumbnails(t)];

            case 1:
              return [2, n.sent()];
          }
        });
      });
    }

    const ERROR_STRING = "ERRORED";
    const BASE_URL = globalThis.process && globalThis.process.env.NODE_ENV === "production" ? "https://vueprep.com" : "http://localhost:8000";

    function fetchData(url, body) {
      return fetch(url, {
        method: "POST",
        mode: "cors",
        headers: {
          "X-CSRFToken": $("input[name='csrfmiddlewaretoken']").val()
        },
        body
      });
    }

    function setBarGraph($wrapper, data) {
      // Get Max Value From Array
      var traits = Object.keys(data);
      var values = Object.values(data); // Set width of bar to percent of max value

      $wrapper.find(".bar-row").each((i, el) => {
        let value = +values[i];
        let $barRow = $(el);
        let $bar = $barRow.find(".bar"); // Set Width & Add Class to show

        $bar.width(value + "%");
        $barRow.find(".label").text(traits[i]);
        $barRow.find(".number").text(value);
      });
    }
    function animateBarGraph($wrapper) {
      $wrapper.find(".bar-row").each((i, el) => {
        let $barRow = $(el);
        $barRow.find(".bar").addClass("in");
      });
    } // export function displayResults() {
    //   console.log("displaying results..");
    //   $("#btnUpload").prop("disabled", false);
    //   $("#spinner").hide();
    //   $(".result").show();
    //   // var resJson = JSON.parse(res);
    //   var resJson = {
    //     "recording quality": {
    //       loudness: "good",
    //       speech: "not detected",
    //       facial: "good",
    //       occlusion: "yes",
    //       blurness: "blurry",
    //       lighting: "good",
    //     },
    //     "facial attributes": {
    //       gender: "male",
    //       age: 29,
    //       beauty: 56,
    //       smile: 3,
    //       emotion: "neutral",
    //       eyegaze: "not center",
    //     },
    //     "personality traits": {
    //       interview: 64,
    //       openness: 68,
    //       conscientiousness: 64,
    //       extraversion: 64,
    //       agreeableness: 66,
    //       neuroticism: 65,
    //     },
    //   };
    //   // resJson = JSON.parse(resJson);
    //   console.log(resJson);
    //   // const facial = resJson["facial attributes"];
    //   //
    //   // for (const trait in facial) {
    //   //   console.log(`${trait}: ${facial[trait]}`);
    //   //   $("#facial-result").append(`<p>${trait}: ${facial[trait]}</p>`);
    //   // }
    //   var traits = [];
    //   var values = [];
    //   const personality = resJson["personality traits"];
    //   for (const trait in personality) {
    //     console.log(`${trait}: ${personality[trait]}`);
    //     traits.push(trait);
    //     values.push(personality[trait]);
    //   }
    //   generateBarGraph("#personality", traits, values);
    //   // traits = [];
    //   // values = [];
    //   // const social = resJson["social evaluations"];
    //   // for (const trait in social) {
    //   //   console.log(`${trait}: ${social[trait]}`);
    //   //   traits.push(trait);
    //   //   values.push(social[trait]);
    //   // }
    //   // generateBarGraph("#social", traits, values);
    //   // $(".cta-1").css("display", "block");
    // }

    function setGrade($grade, score, grade) {
      $grade.text(grade);

      if (Number(score) > 60) {
        $grade.addClass("text-success");
      } else {
        $grade.addClass("text-warning");
      }
    }

    class VideoSender {
      constructor(q_count, i_id, should_predict = false, should_transcribe = false) {
        this.q_count = q_count;
        this.r_count = 0;
        this.i_id = i_id;
        this.should_predict = false;
        this.should_transcribe = should_transcribe;
      }

      async sendVideo(blob, q) {
        if (!this.i_id) {
          console.error("cannot find current interview");
          return;
        }

        if (!q) {
          console.error("cannot find current question");
          return;
        }

        var q_content = q.content.replace(/[^\w\s]/gi, "").replace(/ /g, "_").toLowerCase();
        var thumbnails;
        var u_id = $("#u_id").val();
        var videoName = `browser_recorded_${q_content}.webm`;
        var thumbName = `thumb_${q_content}.jpg`; // make form data

        var formData = new FormData();
        formData.append("video", blob, videoName);
        formData.append("q_id", q.id);
        formData.append("i_id", this.i_id);

        try {
          thumbnails = await p(blob, {
            quality: 0.8,
            scale: 0.5,
            start: 1,
            end: 1
          });
          formData.append("thumb", thumbnails[0].blob, thumbName);
        } catch (e) {
          console.error(e);
          this.handleError(q);
          return;
        }

        var postRes, getRes;

        try {
          console.log("sending video...", this.i_id);

          if (this.should_predict) {
            formData.append("should_predict", true);
          }

          postRes = await fetchData(`${BASE_URL}/api/process-interview-question`, formData);

          if (postRes && postRes.error) {
            this.handleError(q);
            return;
          }
        } catch (e) {
          console.error("post res errored:", e);
          this.handleError(q);
          return;
        }

        let url = new URL(`${BASE_URL}/interview-question`);
        url.search = new URLSearchParams({
          i_id: this.i_id,
          q_id: q.id
        }).toString();

        try {
          getRes = await fetch(url);
          getRes = await getRes.json();
          console.log(getRes);

          if (getRes && getRes.error) {
            console.error("get res errored", getRes.error);
            this.handleError(q);
            return;
          }

          this.setResult(getRes, q);
        } catch (e) {
          console.error("get res errored", e);
          this.handleError(q);
          return;
        } // if function didn't return here, question requests success


        this.r_count++;
        console.log("this.r_count", this.r_count);
        if (this.r_count == this.q_count) this.setFinalResult();
      }

      handleError(q) {
        var $resultWrapper = $("#result-" + q.id);
        $resultWrapper.find(".placeholder").hide();
        $resultWrapper.find(".error").show();
        this.r_count++;
        console.log("this.r_count", this.r_count);
      }

      setResult(res, q) {
        console.log(res);

        if (res && res.interview_question) {
          var $resultWrapper = $("#result-" + res.q_id);
          var $resultContent = $resultWrapper.find(".card-content");
          $resultContent.show();
          $resultContent.find("video").attr("src", res.interview_question.video_url);
          $resultWrapper.find(".placeholder").hide();

          if (this.should_predict) {
            if (res.interview_question.score !== ERROR_STRING) {
              setBarGraph($resultContent.find(".chart"), JSON.parse(res.interview_question.prediction).personality_traits);
              animateBarGraph($resultContent.find(".chart"));
              setGrade($resultContent.find(".score"), res.interview_question.score, res.interview_question.grade);
              $resultWrapper.find(".score-wrapper").show();
            } else {
              $resultContent.find(".error-analyze").show();
            }
          }
        } else {
          $("#result-" + q.id).find(".error").show();
        }
      }

      async finishInterview() {
        var formData = new FormData();
        formData.append("i_id", this.i_id);
        return await fetchData(`${BASE_URL}/mock-interview`, formData);
      }

      async setFinalResult() {
        var $grade = $("#final-score");
        var res;

        try {
          res = await this.finishInterview();
          res = await res.json();
          console.log(res);

          if (this.should_predict) {
            if (res.error || res.grade == ERROR_STRING) {
              $grade.text(ERROR_STRING).addClass("text-danger");
            } else {
              setGrade($grade, res.score, res.grade);
              $(".feedback-wrapper").show();
              $(".feedback-content").text(res.interpretation);
            }
          }
        } catch (e) {
          console.error(e);
          $grade.text("ERROR").addClass("text-danger");
        }
      }

    }

    async function mockInterview() {
      // setup
      var questions;
      var currentQuestion;
      var videoSender;
      var mediaRecorder;
      var timerInterval = null;
      var timeSecPerQuestion = 60;
      var breakSecPerQuestion = 15;
      var $questionDisplay = $("#question");
      var $timeDisplay = $("#timer");
      var $videoDisplay = $(".video-wrapper");
      var $btnBegin = $("#btn-begin");
      var $btnRecord = $("#btn-record");
      var $btnNext = $("#btn-next");
      var $btnResults = $("#btn-results");
      var $notice = $("#notice");
      var $readyGO = $("#ready-go"); // functions

      function init() {
        $("#video-switch").on("change", () => {
          $("#video-recorder").toggleClass("transparent");
        });
        $(".selections .custom-select").on("change", () => {
          if ($("#criterion-select").val() != null) $btnBegin.prop("disabled", false);
        });
      }

      async function setupWebcam() {
        var chunks = [];
        mediaRecorder = await webcamDriver("#video-recorder");

        mediaRecorder.ondataavailable = ev => {
          chunks.push(ev.data);
        };

        mediaRecorder.onstop = ev => {
          try {
            var blob = new Blob(chunks, {
              type: "video/webm"
            });
            videoSender.sendVideo(blob, currentQuestion);
          } catch (e) {
            console.error(e);
          } finally {
            chunks = [];
            startBreak(breakSecPerQuestion);
          }
        };

        document.addEventListener("onbeforeunload", () => {
          videoSender.finishInterview();
        });
      }

      function setupResultCards() {
        $(".card").each((i, card) => {
          let $card = $(card);

          if (questions[i]) {
            $card.attr("id", "result-" + questions[i].id);
            $card.find(".card-title").text(questions[i].content);
          } else {
            $card.remove();
          }
        });
      }

      async function beginInterview() {
        $questionDisplay.text("Session starting"); // create interview in

        var formData = new FormData();
        formData.append("crit_id", $("#criterion-select").val());
        var res = await fetchData(`${BASE_URL}/mock-interview`, formData);
        res = await res.json();
        questions = JSON.parse(res.questions);
        var minutes = $("#minute-select").val();
        timeSecPerQuestion *= minutes;
        console.log(minutes, timeSecPerQuestion);

        if (!questions.length) {
          $questionDisplay.html("<p>We don't have any questions matching your filters... </p> <p>Please try something else!</p>");
          return;
        }

        setupResultCards(); // init video sender

        videoSender = new VideoSender(questions.length, res.i_id); // start webcam

        await setupWebcam(); // update register link

        if ($("#interview-register")[0]) {
          let $btnReg = $("#interview-register");
          $btnReg.attr("href", $btnReg.attr("href") + `?i_id=${res.i_id}`);
        } // start a quick countdown


        var count = 3;

        while (count !== 0) {
          $questionDisplay.append(".");
          await sleep(1);
          count--;

          if (count === 0) {
            // show first question and start break timer
            startBreak(breakSecPerQuestion);
            $videoDisplay.fadeIn();
          }
        }
      }

      function sleep(s) {
        return new Promise(resolve => setTimeout(resolve, s * 1000));
      }

      function setNextQuestion() {
        if (questions.length) {
          currentQuestion = questions.shift();
          $questionDisplay.text(currentQuestion.content);
        }
      }

      function setTimerValue(duration) {
        var minutes = parseInt(duration / 60, 10);
        var seconds = parseInt(duration % 60, 10);
        minutes = minutes < 10 ? "0" + minutes : minutes;
        seconds = seconds < 10 ? "0" + seconds : seconds;
        $timeDisplay.text(minutes + ":" + seconds);
      }

      function startBreak(duration) {
        if (questions.length) {
          setNextQuestion();
        } else {
          return;
        }

        $notice.show();
        $btnNext.hide();
        $btnRecord.show();
        $notice.text("Take some time to read the question above.");
        setTimerValue(duration);
        timerInterval = setInterval(async () => {
          duration--;
          setTimerValue(duration);

          if (duration < 5) {
            $notice.text("Get ready! Your response starts in " + (duration + 1) + ".");
          }

          if (duration === 0) {
            await sleep(1);
            stopBreakAndStartCountDown();
          }
        }, 1000);
      }

      function stopBreakAndStartCountDown() {
        clearInterval(timerInterval);
        $btnRecord.hide();
        $notice.hide();
        startCountDown(timeSecPerQuestion);
        $readyGO.show();
        setTimeout(function () {
          $readyGO.fadeOut('slow', 'linear');
        }, 400);

        if (questions.length) {
          $btnNext.show();
        } else {
          $btnResults.show();
        }
      }

      function startRecorder() {
        if (mediaRecorder.state !== "recording") {
          $(".circle.blinking").show();
          mediaRecorder.start();
        }
      }

      async function stopRecorder() {
        if (mediaRecorder.state === "recording") {
          $(".circle.blinking").hide();
          await mediaRecorder.stop();
        }
      }

      async function startCountDown(duration) {
        startRecorder();
        setTimerValue(duration);
        timerInterval = await setInterval(async () => {
          duration--;
          setTimerValue(duration);

          if (duration === 0) {
            await sleep(1);
            await stopCountDown();
          }
        }, 1000);
      }

      async function stopCountDown() {
        setTimerValue(0);
        await stopRecorder();
        clearInterval(timerInterval);
      } // register events


      $btnBegin.on("click", async () => {
        $btnBegin.hide();
        $(".selections").hide();
        beginInterview();
      });
      $btnRecord.on("click", () => {
        stopBreakAndStartCountDown();
      });
      $btnNext.on("click", async () => {
        await stopCountDown();
      });
      $btnResults.on("click", async () => {
        await stopCountDown();
        $videoDisplay.fadeOut();
        $questionDisplay.fadeOut();
        $(".results-wrapper").fadeIn();
      });
      init();
    }

    function home() {
      function init() {
        $(".on-view").waypoint(function () {
          $(this.element).css("opacity", 1);
          $(this.element).addClass("animate__fadeInUp");
        }, {
          offset: "100%"
        });
      }

      init();
    }

    function interviewDetails() {
      $(".btn-predict-question").on("click", async e => {
        var $this = $(e.target);
        var i_q_id = $this.data("id");
        var formData = new FormData();
        var $resultsWrapper = $("#interview-question-" + i_q_id).find(".results-wrapper");
        var $loader = $resultsWrapper.find(".loader");
        var $status = $resultsWrapper.find(".status");
        var $grade = $resultsWrapper.find(".grade"); // after click

        $loader.show();
        $status.text("Analyzing...");
        $this.hide(); // make prediction

        formData.append("i_q_id", i_q_id);
        var res = await fetchData(`${BASE_URL}/api/predict-interview-question`, formData);
        res = await res.json();
        console.log(res); // after result

        if (res) {
          $status.hide();
          $loader.hide();
        }

        var {
          interview_question,
          interview
        } = res;

        if (interview_question.error || interview_question.grade == ERROR_STRING) {
          $resultsWrapper.find(".video-error").show();
        } else {
          setGrade($grade, interview_question.score, interview_question.grade);
          setBarGraph($(`#modal-${interview_question.id} .chart`), JSON.parse(interview_question.prediction).personality_traits);
          $resultsWrapper.find(".result").show();
          $resultsWrapper.find(".modal-launch").show();
        }

        if (!interview.error && interview.grade != ERROR_STRING) {
          var $wrapper = $(".interview-grade");
          setGrade($wrapper.find(".grade"), interview.score, interview_question.grade);
          $wrapper.show();
        }

        if (!interview.error && interview.interpretation != ERROR_STRING) {
          $(".interview-interpretation-wrapper").show();
          $(".interview-interpretation").text(interview.interpretation);
        }
      });
      $(".modal-launch").on("click", e => {
        var $this = $(e.target);
        var i_q_id = $this.data("id");
        animateBarGraph($(`#modal-${i_q_id} .chart`));
      });
      $(".generate-transcript").on("click", async e => {
        var $this = $(e.target);
        var i_q_id = $this.data("id");
        var $wrapper = $(`.transcript#transcript-${i_q_id}`);
        $this.hide();
        $wrapper.find(".loader").show();
        $wrapper.find(".status").text("Generating...");
        let res;

        try {
          let formData = new FormData();
          formData.append("i_q_id", i_q_id);
          res = await fetchData(`${BASE_URL}/api/transcribe-interview-question`, formData);
          res = await res.json();
          $wrapper.html(`<p>${res.transcript}</p>`);
        } catch (e) {
          console.error(e);
        }
      });
    }

    $(document).ready(function () {
      // page functions
      if ($("#mock-interview")[0]) {
        mockInterview();
      }

      if ($("#interview-details")[0]) {
        interviewDetails();
      }

      if ($("#register")[0]) ;

      if ($("#home")[0]) {
        home();
      } // $("#videoFile").on("change", function() {
      //   //get the file name
      //   var fileName = $(this)
      //     .val()
      //     .replace(/^.*\\/, "");
      //   //replace the "Choose a file" label
      //   $(this)
      //     .next(".custom-file-label")
      //     .html(fileName);
      // });
      //
      // $("input:file").on("change", function() {
      //   if ($(this).val()) {
      //     $("input:submit").attr("disabled", false);
      //   }
      // });
      //
      // $("#background-mode input").on("change", function() {
      //   $("body").addClass(".body");
      //   alert($("input[name=radioName]:checked", "#background-mode").val());
      // });
      //
      // $("input[type='button']").click(function() {
      //   var radioValue = $("input[name='gender']:checked").val();
      //   if (radioValue) {
      //     alert("Your are a - " + radioValue);
      //   }
      // });
      //
      // $("#btnUpload").click(async ev => {
      //   //stop submit the form, we will post it manually.
      //   ev.preventDefault();
      //
      //   $(".UploadForm").hide();
      //
      //   // Create an FormData object
      //   var fileInput = document.getElementById("videoFile");
      //   var file = fileInput.files[0];
      //   var formData = new FormData();
      //   formData.append("file", file);
      //   console.log("formData", formData.entries());
      //
      //   // disabled the submit button
      //   $("#btnUpload").prop("disabled", true);
      //   $("#spinner").show();
      //   $(".result").hide();
      //   let res;
      //   try {
      //     res = await sendVideo(formData);
      //   } catch (err) {
      //     throw err;
      //   }
      //   displayResults(res);
      // });

    });

})));
