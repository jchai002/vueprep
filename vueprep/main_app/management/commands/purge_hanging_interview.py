from django.core.management.base import BaseCommand, CommandError
from main_app.models import Interview
from datetime import datetime, timedelta
from django.utils import timezone
from vueprep.settings import STATUS

class Command(BaseCommand):
    help = 'Delete interviews still in progress after 1 day'

    def handle(self, *args, **options):
        Interview.objects.filter(updated__lte=timezone.now()-timedelta(days=1), status=STATUS['IN_PROGRESS']).delete()
        self.stdout.write('Deleted interviews still in progress after 1 day')
